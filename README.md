A small Docker image to test how Python handles signals.

Run `./release.sh <image-tag>` to build and push to a registry that is accessible from your k8s cluster (could be minikube, in which case you don't have to push, just build).

One can then run it in k8s with something like:

```
kubectl run <image-name> --image=<image-tag> --image-pull-policy='Always' --restart='Never' -- python -u -m main
```

## k8s signals
On deletion of the Pod, k8s will send a SIGTERM, which is different from a SIGINT that is translated into `KeyboardInterrupt` by Python. Instead, one must handle this signal to terminate gracefully, as after the (configurable) grace period, k8s will send a SIGKILL which cannot be caught by Python and will immediately kill the process.

Playing around with the `main.py` script arguments can show what happens in different situations.
