import argparse
from signal import signal, SIGINT, SIGTERM
import sys
from time import sleep

valid_signals = {SIGINT, SIGTERM}


if __name__ == "__main__":

    p = argparse.ArgumentParser()
    p.add_argument(
        "--exit", type=int, default=None, help="Quit after 1s with this exit code"
    )
    p.add_argument(
        "--no-listen", action="store_true", help="Do not listen to signal handlers"
    )
    p.add_argument(
        "--keyboard-interrupt",
        action="store_true",
        help="Raise a keyboard interrupt instead of exiting using sys.exit()",
    )
    args = p.parse_args()

    def handler(signal_received, frame):
        # .strsignal only landed in 3.8
        signal_name = next(filter(lambda x: x == signal_received, valid_signals)).name
        print(f"Detected {signal_name}, exiting now")
        if args.keyboard_interrupt:
            raise KeyboardInterrupt()
        sys.exit(0)

    if not args.no_listen:
        signal(SIGINT, handler)
        signal(SIGTERM, handler)

    while 1:
        print("Still going, nothing to see here...")
        sleep(1)
        if args.exit is not None:
            sys.exit(args.exit)
