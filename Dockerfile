FROM python:3.8-alpine

COPY main.py /main.py

CMD python -u /main.py
